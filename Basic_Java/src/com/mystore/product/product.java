package com.mystore.product;

// Class declaration : AccessLevel Class className 

public class product {

	//Memeber variables or fields Syntex
	// AccessLevel (static) dataType variable/field name
	
	public String name;
	public double price;
	
	//Methods or Functions
	// AccessLevel (static) returnDataType MethodName(DataType arug1,... DataType arugN)
	
	public void purchase(int quantity)
	{
	System.out.println("Display Product name : " + quantity);
	}
	
	public double price(int quantity)
	{
	return (quantity*price);
	}
}

